package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DocumentPictureRequestDTO implements Serializable {

    private String personalIdentificationNumber;
    private String id;
    private Boolean isAnverso;

}