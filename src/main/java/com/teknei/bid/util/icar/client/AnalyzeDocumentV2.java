
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="User" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GetDocumentIdForMerging" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DocInV2" type="{http://IdCloud.iCarVision/WS/}DocumentCheckInV2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "company",
    "user",
    "pwd",
    "getDocumentIdForMerging",
    "docInV2"
})
@XmlRootElement(name = "AnalyzeDocumentV2")
public class AnalyzeDocumentV2 {

    @XmlElement(name = "Company")
    protected String company;
    @XmlElement(name = "User")
    protected String user;
    @XmlElement(name = "Pwd")
    protected String pwd;
    @XmlElement(name = "GetDocumentIdForMerging")
    protected boolean getDocumentIdForMerging;
    @XmlElement(name = "DocInV2")
    protected DocumentCheckInV2 docInV2;

    /**
     * Obtiene el valor de la propiedad company.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompany() {
        return company;
    }

    /**
     * Define el valor de la propiedad company.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompany(String value) {
        this.company = value;
    }

    /**
     * Obtiene el valor de la propiedad user.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Define el valor de la propiedad user.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Obtiene el valor de la propiedad pwd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * Define el valor de la propiedad pwd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwd(String value) {
        this.pwd = value;
    }

    /**
     * Obtiene el valor de la propiedad getDocumentIdForMerging.
     * 
     */
    public boolean isGetDocumentIdForMerging() {
        return getDocumentIdForMerging;
    }

    /**
     * Define el valor de la propiedad getDocumentIdForMerging.
     * 
     */
    public void setGetDocumentIdForMerging(boolean value) {
        this.getDocumentIdForMerging = value;
    }

    /**
     * Obtiene el valor de la propiedad docInV2.
     * 
     * @return
     *     possible object is
     *     {@link DocumentCheckInV2 }
     *     
     */
    public DocumentCheckInV2 getDocInV2() {
        return docInV2;
    }

    /**
     * Define el valor de la propiedad docInV2.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentCheckInV2 }
     *     
     */
    public void setDocInV2(DocumentCheckInV2 value) {
        this.docInV2 = value;
    }

}
