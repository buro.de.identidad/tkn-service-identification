
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnalyzeDocumentV2Result" type="{http://IdCloud.iCarVision/WS/}DocumentCheckOutV2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analyzeDocumentV2Result"
})
@XmlRootElement(name = "AnalyzeDocumentV2Response")
public class AnalyzeDocumentV2Response {

    @XmlElement(name = "AnalyzeDocumentV2Result")
    protected DocumentCheckOutV2 analyzeDocumentV2Result;

    /**
     * Obtiene el valor de la propiedad analyzeDocumentV2Result.
     * 
     * @return
     *     possible object is
     *     {@link DocumentCheckOutV2 }
     *     
     */
    public DocumentCheckOutV2 getAnalyzeDocumentV2Result() {
        return analyzeDocumentV2Result;
    }

    /**
     * Define el valor de la propiedad analyzeDocumentV2Result.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentCheckOutV2 }
     *     
     */
    public void setAnalyzeDocumentV2Result(DocumentCheckOutV2 value) {
        this.analyzeDocumentV2Result = value;
    }

}
