package com.teknei.bid.util.icar;

import com.teknei.bid.util.icar.client.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.teknei.bid.persistence.entities.CfgGeneralConfiguration;
import com.teknei.bid.persistence.repository.CfgGeneralConfigurationRepository;
import java.util.UUID;
/**
 * Created by Amaro on 14/08/2017.
 */
@Component
public class IcarManager {

    @Value("${tkn.icar.path}")
    private String icarXmlFilePath;
    @Value("${tkn.icar.wsdl}")
    private String icarWSDL;
    @Value("${tkn.icar.company}")
    private String icarCompany;
    @Value("${tkn.icar.user}")
    private String icarUser;
    @Value("${tkn.icar.password}")
    private String icarPassword;

    @Autowired
	private CfgGeneralConfigurationRepository cfg;

    private byte[] icarFileContent = new byte[4096];

    private static final Logger log = LoggerFactory.getLogger(IcarManager.class);

    @PostConstruct
    private void init() {
        Path icarPath = Paths.get(icarXmlFilePath);
        try {
            icarFileContent = Files.readAllBytes(icarPath);
        } catch (IOException e) {
            log.error("Error reading config file from ICAR in path: {} with message: {}", icarXmlFilePath, e.getMessage());
        }
    }

    public JSONObject getDocumentInfo(String base64ImageFront, String base64ImageBack) {
        byte[] front = Base64Utils.decodeFromString(base64ImageFront);
        byte[] back = Base64Utils.decodeFromString(base64ImageBack);
        DocumentCheckInV2 documentCheckInV2 = new DocumentCheckInV2();
        DocumentImage documentImage1 = new DocumentImage();
        documentImage1.setImage(front);
        documentImage1.setDeviceInfo(icarFileContent);
        documentImage1.setFiletype("jpeg");
        documentImage1.setImageResolution(150);
        documentCheckInV2.setImage1(documentImage1);
        DocumentImage documentImage2 = new DocumentImage();
        documentImage2.setImage(back);
        documentImage2.setDeviceInfo(icarFileContent);
        documentImage2.setFiletype("jpeg");
        documentImage2.setImageResolution(150);
        documentCheckInV2.setImage2(documentImage2);
        return getDocumentInfo(documentCheckInV2);
    }

    public JSONObject getDocumentInfo(DocumentCheckInV2 documentCheckInV2) {
        JSONObject jsonResponse = new JSONObject();
        try {
            //--- Modificacion Obtener wsdl de base de datos cambio de url 
            try {
				log.info("INFO:  configuraciones viejas ---->> ");
				log.info("INFO:  icarWSDL: "+icarWSDL);
				log.info("INFO:  icarUser: "+icarUser);
				log.info("INFO:  icarPassword: "+icarPassword);
				log.info("INFO:  icarCompany: "+icarCompany);
				log.info("INFO:  Cargando nuevas configuraciones de Base ---->> ");
				List<CfgGeneralConfiguration> configList = cfg.findByCfgGroup("ocr");
				for (CfgGeneralConfiguration obj : configList) {
					log.info("INFO:  ---->> " + obj.getParameter()+ ":" + obj.getValue());
					if (obj.getParameter().equals("wsdl")) {
						this.icarWSDL = obj.getValue();
					}
					if (obj.getParameter().equals("user")) {
						this.icarUser = obj.getValue();
					}
					if (obj.getParameter().equals("pass")) {
						this.icarPassword = obj.getValue();
					}
					if (obj.getParameter().equals("company")) {
						this.icarCompany = obj.getValue();
					}
				}
			} catch (Exception e) {
				log.error("ERROR AL OBTENER VALORES DE CONECCION ICAR : ",e);
			}
    		log.info("ICAR icarWSDL: {}", icarWSDL);
            //------------------------------------------------------------------
            //Se invoca al servicio Web
            DocumentCheckOutV2 documentOutV2Ex = null;
            WsDocument ws = new WsDocument(new URL(icarWSDL));
            try {
                documentOutV2Ex = ws.getWsDocumentSoap12().analyzeDocumentV2Ex(icarCompany, icarUser, icarPassword, true, documentCheckInV2);
               
            try{ log.info("ICAR icarWSDL RESULT>>>getFields : {}",  documentOutV2Ex.getFields() != null ? new JSONObject(documentOutV2Ex.getFields()).toString()+"": "");} catch (Exception e) { }
            try{log.info("ICAR icarWSDL RESULT>>>getReference: {}",   documentOutV2Ex.getReference() != null ? new JSONObject(documentOutV2Ex.getReference()).toString()+"": "");} catch (Exception e) { }
             try{ log.info("ICAR icarWSDL RESULT>>>getWarning: {}", documentOutV2Ex.getWarning() != null ?  new JSONObject(documentOutV2Ex.getWarning()).toString()+"": "");} catch (Exception e) { }
            try{log.info("ICAR icarWSDL RESULT>>>getMessages: {}", documentOutV2Ex.getMessages() != null ?  new JSONObject(documentOutV2Ex.getMessages()).toString()+"": "");} catch (Exception e) { }
             try{log.info("ICAR icarWSDL RESULT>>>getResult: {}", documentOutV2Ex.getResult() != null ?  new JSONObject(documentOutV2Ex.getResult()).toString()+"": "");} catch (Exception e) { }
             try{log.info("ICAR icarWSDL RESULT>>>getDocumentIdForMerging: {}", documentOutV2Ex.getDocumentIdForMerging() != null ?  new JSONObject(documentOutV2Ex.getDocumentIdForMerging()).toString()+"": "");} catch (Exception e) { }
                
            } catch (Exception e) {
                log.error("Error al configurar la petición al servicio de Icar (analyzeDocumentV2Ex)!!!");
                throw e;
            }
            JSONObject document = new JSONObject();
            jsonResponse.put("document", document);
            //Se analiza la respuesta
            ArrayOfMessage messages = documentOutV2Ex.getMessages();
            if (messages != null && !messages.getMessage().isEmpty()) {
                int contMessage = 1;
                for (Message message : messages.getMessage()) {
                    //System.out.println(String.format("\tICAR - Código de mensaje: %d, %s.", message.getCode(), message.getDescription()));
                    jsonResponse.put("warning_" + contMessage++, String.format("\tICAR - Código de mensaje: %d, %s.", message.getCode(), message.getDescription()));
                    log.warn(String.format("ICAR - Código de mensaje: %d, %s.", message.getCode(), message.getDescription()));
                }
            } else {
                //DocumentCheckOutV2 rowDocOut = documentOutV2Ex;
                //System.out.println(String.format("Resultado: %s", rowDocOut.getResult().toString()));
                //if (!documentOutV2Ex.getWarning().isEmpty())
                //log.warn(String.format("ICAR Warning: %s", rowDocOut.getWarning()));
                jsonResponse.put("warning", documentOutV2Ex.getWarning());
                String fakeuiid = UUID.randomUUID()+"";
                jsonResponse.put("scanId", fakeuiid);//documentOutV2Ex.getDocumentIdForMerging());
                document.put("scanId", fakeuiid);//documentOutV2Ex.getDocumentIdForMerging());

//				System.out.println("Propiedades capturadas:");
                //Se obtienen las propiedades extraídas
                for (Field rowProp : documentOutV2Ex.getFields().getField()) {
                    //System.out.println(String.format("\tTipo: %s,\tCódigo: %s,\tDescrip: %s,\tValor: %s", rowProp.getType(), rowProp.getCode(), rowProp.getDescription(), rowProp.getValue()));
                    //Se añaden al documento
                    switch ((String) rowProp.getType().value()) {
                        case "Normal":
                            switch (rowProp.getCode()) {
                                case "TYPE":
                                    document.put("TYPE", rowProp.getValue());
                                    break;
                                case "SIDE":
                                    document.put("SIDE", rowProp.getValue());
                                    break;

                                case "EXPEDITOR":
                                    document.put("issuingState", rowProp.getValue());
                                    break;
                                case "NATIONALITY":
                                    document.put("nationality", rowProp.getValue());
                                    break;
                                case "NAME":
                                    document.put("name", rowProp.getValue());
                                    break;
                                case "SURNAME":
                                    if (!document.has("firstSurname")) {
                                        document.put("firstSurname", rowProp.getValue());
                                        document.put("surname", rowProp.getValue());
                                    } else {
                                        document.put("secondSurname", rowProp.getValue());
                                        document.put("surname", document.getString("firstSurname") + " " + rowProp.getValue());
                                    }
                                    break;
                                case "ID_NUMBER":
                                    document.put("ID_NUMBER", rowProp.getValue());
                                    break;
                                case "DOC_NUMBER":
                                    document.put("documentNumber", rowProp.getValue());
                                    break;
                                case "ADDRESS":
                                    if (document.has("address"))
                                        document.put("address", document.getString("address") + "\n" + rowProp.getValue());
                                    else
                                        document.put("address", rowProp.getValue());
                                    break;
                                case "BIRTHDATE":
                                    document.put("dateOfBirth", rowProp.getValue());
                                    break;
                                case "BIRTHPLACE":
                                    document.put("BIRTHPLACE", rowProp.getValue());
                                    break;
                                case "EXPIRY":
                                    if (!document.has("dateOfExpiry"))
                                        document.put("dateOfExpiry", rowProp.getValue());
                                    else
                                        document.put("vigencia", rowProp.getValue());
                                    break;
                                case "SEX":
                                    document.put("gender", (rowProp.getValue().equals("F") ? "M" : "H"));
                                    break;
                                case "AGE":
                                    document.put("AGE", rowProp.getValue());
                                    break;
                                case "STATE":
                                    document.put("stateCode", rowProp.getValue());
                                    break;
                                case "LOCALITY":
                                    document.put("LOCALITY", rowProp.getValue());
                                    break;
                                case "ELECTOR_ID":
                                    document.put("claveElector", rowProp.getValue());
                                    break;
                                case "REGISTRY_YEAR":
                                    document.put("REGISTRY_YEAR", rowProp.getValue());
                                    break;
                                case "MUNICIPALITY":
                                    document.put("MUNICIPALITY", rowProp.getValue());
                                    break;
                                case "EXPEDITION_DATE":
                                    document.put("EXPEDITION_DATE", rowProp.getValue());
                                    break;
                                case "SECTION":
                                    document.put("section", rowProp.getValue());
                                    break;
                                case "CURP":
                                    document.put("curp", rowProp.getValue());
                                    break;
                                case "CRC_SECTION":
                                    document.put("CRC_SECTION", rowProp.getValue());
                                    break;
                                case "REGISTRY_FOLIO":
                                    document.put("REGISTRY_FOLIO", rowProp.getValue());
                                    break;
                                case "MRZ":
                                    if (!document.has("MRZ"))
                                        document.put("MRZ", rowProp.getValue());
                                    else
                                        document.put("MRZ", document.getString("MRZ") + "\n" + rowProp.getValue());
                                    break;
                                case "MODEL_ID":
                                    document.put("documentType", rowProp.getValue());
                                    jsonResponse.put("documentType", rowProp.getValue());
                                    break;
                                case "SIDES_NUMBER":
                                    document.put("SIDES_NUMBER", rowProp.getValue());
                                    break;
                                case "ICAR_RESULT_DOCUMENT_ID":
                                    jsonResponse.put("documentType", rowProp.getValue());
                                    break;
                            }

                            break;
                        case "Validation":
                            switch (rowProp.getCode()) {
                                case "TEST_MRZ_GLOBAL_INTEGRITY":
                                    document.put("TEST_MRZ_GLOBAL_INTEGRITY", rowProp.getValue());
                                    break;
                                case "TEST_MRZ_FIELDS_INTEGRITY DOC_NUMBER":
                                    document.put("TEST_MRZ_FIELDS_INTEGRITY_DOC_NUMBER", rowProp.getValue());
                                    break;
                                case "TEST_MRZ_FIELDS_INTEGRITY BIRTHDATE":
                                    document.put("TEST_MRZ_FIELDS_INTEGRITY_BIRTHDATE", rowProp.getValue());
                                    break;
                                case "TEST_MRZ_FIELDS_INTEGRITY EXPIRY":
                                    document.put("TEST_MRZ_FIELDS_INTEGRITY_EXPIRY", rowProp.getValue());
                                    break;
                                case "TEST_CORRESPONDENCE_BARCODE_MRZ CRC_SECTION":
                                    document.put("TEST_CORRESPONDENCE_BARCODE_MRZCRC_SECTION", rowProp.getValue());
                                    break;
                                case "TEST_COLOR_IMAGE":
                                    document.put("TEST_COLOR_IMAGE", rowProp.getValue());
                                    break;
                                case "TEST_VIZ_VOTER_CODE_NUMBER ":
                                    document.put("TEST_VIZ_VOTER_CODE_NUMBER", rowProp.getValue());
                                    break;
                                case "TEST_EXPIRY_DATE":
                                    document.put("TEST_EXPIRY_DATE", rowProp.getValue());
                                    break;
                                case "TEST_CORRESPONDENCE_VISIBLE_MRZ NAME":
                                    document.put("TEST_CORRESPONDENCE_VISIBLE_MRZ_NAME", rowProp.getValue());
                                    break;
                                case "TEST_CORRESPONDENCE_VISIBLE_MRZ SURNAME":
                                    document.put("TEST_CORRESPONDENCE_VISIBLE_MRZ_SURNAME", rowProp.getValue());
                                    break;
                                case "TEST_CORRESPONDENCE_VISIBLE_MRZ BIRTHDATE":
                                    document.put("TEST_CORRESPONDENCE_VISIBLE_MRZ_BIRTHDATE", rowProp.getValue());
                                    break;
                                case "TEST_CORRESPONDENCE_VISIBLE_MRZ SEX":
                                    document.put("TEST_CORRESPONDENCE_VISIBLE_MRZ_SEX", rowProp.getValue());
                                    break;
                                case "TEST_SIDE_CORRESPONDENCE":
                                    document.put("TEST_SIDE_CORRESPONDENCE", rowProp.getValue());
                                    break;
                                case "TEST_CORRESPONDENCE_BARCODE_MRZ DOC_NUMBER":
                                    document.put("TEST_CORRESPONDENCE_BARCODE_MRZ_DOC_NUMBER", rowProp.getValue());
                                    break;
                                case "TEST_VIZ_EXPEDITION_DATE_COHERENCE":
                                    document.put("TEST_VIZ_EXPEDITION_DATE_COHERENCE", rowProp.getValue());
                                    break;
//						case "TEST_GLOBAL_AUTHENTICITY_RATIO":
//							document.put("TEST_GLOBAL_AUTHENTICITY_RATIO", rowProp.getValue());
//							break;
//						case "TEST_GLOBAL_AUTHENTICITY_VALUE":
//							document.put("TEST_GLOBAL_AUTHENTICITY_VALUE", rowProp.getValue());
//							break;
                            }
                            break;
                        default:
                            log.warn("ICAR property type not recognized: " + rowProp.getType());
                    }
                }
                jsonResponse.put("status", "COMPLETE");
                jsonResponse.put("code", documentOutV2Ex.getResult().toString());
                if (documentOutV2Ex.getResult().toString().equalsIgnoreCase("OK"))
                    jsonResponse.put("description", "GET_DOCUMENT_SUCCESS");
                else
                    jsonResponse.put("description", "GET_DOCUMENT_ERROR");
            }
        } catch (Exception e) {            
            log.error("Error ICAR : ", e);
            jsonResponse.put("status", "COMPLETE");
            jsonResponse.put("description", e.getMessage());
            jsonResponse.put("code", "ERROR");
            log.error("Error in ICAR : {}", e.getMessage());
        }
        return jsonResponse;
    }

    public JSONObject getDocumentInfo(List<byte[]> filesContents, String contentType, int resolution)
            throws Exception {
        Long tsStart = System.currentTimeMillis();
        log.info("Starting upload to ICAR with timestamp: {}", tsStart);
        if(filesContents.size() > 1){
            log.debug("Info from multiple pictures");
        }else{
            log.info("Info from single picture");
        }
        try {
            DocumentCheckInV2 documentInV2Ex = new DocumentCheckInV2();
            try {
                DocumentImage documentImage1 = new DocumentImage();
                byte fileBytes1[] = filesContents.get(0);
                documentImage1.setImage(fileBytes1);
                documentImage1.setDeviceInfo(icarFileContent);
                documentImage1.setFiletype(contentType.substring(contentType.lastIndexOf('/') + 1));
                documentImage1.setImageResolution(resolution);
                documentInV2Ex.setImage1(documentImage1);
                if (filesContents.size() == 2) {
                    DocumentImage documentImage2 = new DocumentImage();
                    byte fileBytes2[] = filesContents.get(1);
                    documentImage2.setImage(fileBytes2);
                    documentImage2.setDeviceInfo(icarFileContent);
                    documentImage2.setFiletype(contentType.substring(contentType.lastIndexOf('/') + 1));
                    documentImage2.setImageResolution(resolution);
                    documentInV2Ex.setImage2(documentImage2);
                }
            } catch (Exception e) {
                log.error("Error configuring ICAR request: {}", e.getMessage());
                throw e;
            }
            JSONObject jsonIcarResponse = getDocumentInfo(documentInV2Ex);
            Long tsResponse = System.currentTimeMillis();
            Long tsPassed = tsResponse - tsStart;
            log.info("Response reached from ICAR at ts: {}", tsResponse);
            log.info("Total time: {}", tsPassed);
            log.info("Response from ICAR in raw: {}", jsonIcarResponse);
            return jsonIcarResponse;
        } catch (Exception e) {
            log.error("Exception icar: {}", e.getMessage());
            return null;
        }
    }

    public byte[] getIcarFileContent(){
        return this.icarFileContent;
    }
}
