package com.teknei.bid.service;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.PersonDataIneTKNRequest;
import com.teknei.bid.util.cecoban.CecobanUtil;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Service
public class IdentificationService {

    @Autowired
    private TasManager tasManager;
    @Autowired
    private CecobanUtil cecobanUtil;

    public byte[] findPicture(DocumentPictureRequestDTO requestDTO) {
        return tasManager.getImage(requestDTO.getId(), requestDTO.getIsAnverso());
    }

    public JSONObject verifyCecoban(PersonDataIneTKNRequest personData) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        JSONObject jsonObject = cecobanUtil.makeRequest(personData);
        return jsonObject;
    }

}