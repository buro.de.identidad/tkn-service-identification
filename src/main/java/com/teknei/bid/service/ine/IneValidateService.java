package com.teknei.bid.service.ine;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class IneValidateService {

	private static final Logger log = LoggerFactory.getLogger(IneValidateService.class);

	public List<String>  validateResponseIne(String validateIneResponse) {
		JSONObject validateIne = new JSONObject(validateIneResponse);
		JSONObject response = validateIne.optJSONObject("response");
		log.info("######>>>> " + response.toString());
		List<String> errorCode = new ArrayList<String>();
	
		String registrationSituation = response.optString("registrationSituation");
		if (registrationSituation.toUpperCase().contains("NO_VIGENTE")) {
			errorCode.add("08");
			log.error("ERROR: NO_VIGENTE");
		} else if(registrationSituation.toUpperCase().contains("DATOS_NO_ENCONTRADOS")) {
			errorCode.add("01");
			log.error("ERROR: DATOS_NO_ENCONTRADOS");
			
		}
		if (!response.optBoolean("name")) {
			errorCode.add("05");
			log.error("ERROR: name dont mach");
		}
		if (!response.optBoolean("fatherLastName")) {
			errorCode.add("05");
			log.error("ERROR: fatherLastName dont mach");
		}
		if (!response.optBoolean("motherLastName")) {
			errorCode.add("05");
			log.error("ERROR: motherLastName dont mach");
		}
		if (!response.optBoolean("registryYear")) {
			errorCode.add("05");
			log.error("ERROR: registryYear dont mach");
		}
		if (!response.optBoolean("curp")) {
			 errorCode.add( "05");
			log.error("ERROR: curp dont mach");
		}
		if (!response.optBoolean("ocr")) {
			errorCode.add("01");
			log.error("ERROR: ocr dont mach");
		}
		if (!response.optBoolean("electorCode")) {
			errorCode.add("01");
			log.error("ERROR: electorCode dont mach");
		}
		if (!response.optBoolean("emissionNumber")) {
			errorCode.add("01");
			log.error("ERROR: emissionNumber dont mach");
		}

//		- Rechazo cuando ninguno de los dos índices supera el 98%
		JSONObject additional = response.optJSONObject("additional");
		log.info("lblancas: additional" + additional.toString());	
		JSONObject biometrics = additional.optJSONObject("biometrics");

		if (biometrics != null &&biometrics.toString().length() > 5) {
			String matchFingerL = "0";
			String matchFingerR = "0";
			if (biometrics.optString("matchFingerLeft").length() > 2) {
				matchFingerL = biometrics.optString("matchFingerLeft");
				matchFingerL = matchFingerL.trim().substring(0, matchFingerL.length() - 1);
				log.info("lblancas: matchFingerL " + matchFingerL);
			} else {
				log.error("No se introdujo la huella Left");
				matchFingerL = "100.0";
			}
			if (biometrics.optString("matchFingerRigth").length() > 2) {
				matchFingerR = biometrics.optString("matchFingerRigth");
				matchFingerR = matchFingerR.trim().substring(0, matchFingerR.length() - 1);
				log.info("lblancas: matchFingerR " + matchFingerR);
			} else {
				log.error("No se introdujo la huella Rigth");
				matchFingerR = "100.0";
			}

			if (Double.parseDouble(matchFingerL) < 98.0 || Double.parseDouble(matchFingerR) < 98.0) {
				// error calidad o coincidencia.
				errorCode.add("07");
				log.error("ERROR: matchFinger low: Left:{} %,  Rigth:{} %", matchFingerL, matchFingerR);
				
			}
		}

		return errorCode;
	}

}
