package com.teknei.bid.service.ine.response;

import lombok.Data;

@Data
public class ValidateIneResponse {
	 CurpValidation CurpValidationObject;
	 private boolean ocr;
	 private boolean name;
	 private boolean fatherLastName;
	 private boolean motherLastName;
	 private boolean electorCode;
	 private boolean emissionNumber;
	 private String registrationSituation;
	 private String stoleLostReport;
	 Additional AdditionalObject;
//	 private boolean registryYear;
//	 private boolean emissionYear;
//	 private boolean curp;
	 
	 @Data
	 public class CurpValidation {
		 private boolean nameMatch;
		 private boolean fatherLastNameMatch;
		 private boolean motherLastNameMatch;
//		 private boolean curpValidateMatch;
		 private boolean curpMatch;
	 }
	 
	 @Data
	 public class Additional {
		 Biometrics BiometricsObject;
		 private String idRequest;		 
		 
		 @Data
		 public class Biometrics {
			 private String matchFingerRigth;
			 private String matchFingerLeft;
		 }
	 }
}
