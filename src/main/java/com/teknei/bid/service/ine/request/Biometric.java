package com.teknei.bid.service.ine.request;

import lombok.Data;

@Data
public class Biometric {

	String type;
//		String dataType;
	String fingerNumber;
	String fingerPrint;

}
