package com.teknei.bid.command;

public interface Command {

    CommandResponse execute(CommandRequest request);

}