package com.teknei.bid.command;

import lombok.Data;

import java.io.Serializable;

import org.json.JSONObject;

@Data
public class CommandResponse implements Serializable{

    private Status status;
    private String desc;
    private long id;
    private String scanId;
    private String documentId;
    private String curp;
    private JSONObject json;

}