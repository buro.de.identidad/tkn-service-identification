package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.util.icar.IcarDataSingleton;
import com.teknei.bid.util.icar.IcarManager;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@Component
public class ParseCredentialCommand implements Command {

    @Autowired
    private IcarManager icarManager;

    private static final Logger log = LoggerFactory.getLogger(ParseCredentialCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse response = new CommandResponse();
        try {
            JSONObject jsonBodyIcar = validateCredentials(request.getFileContent());
            if (jsonBodyIcar == null) {
                throw new IllegalArgumentException("null");
            }
            JSONObject internal = jsonBodyIcar.getJSONObject("document");
            boolean nameFound = internal.has("name");
            boolean surnameFound = internal.has("surname");
            boolean ocrFound = internal.has("MRZ") || internal.has("CRC_SECTION");
            if (nameFound == false || surnameFound == false || ocrFound == false) {
                throw new IllegalArgumentException(internal.toString());
            }
            response.setDesc(jsonBodyIcar.toString());
            response.setScanId(jsonBodyIcar.getString("scanId"));
            response.setStatus(Status.CREDENTIALS_ICAR_OK);
        } catch (IllegalArgumentException ie) {
        	if(ie.getMessage().contains("DOCUMENT NOT RECOGNIZED")) {
        		log.info("Documento no reconocido: {}", ie.getMessage());
                response.setStatus(Status.CREDENTIALS_INVALID);
                response.setDesc(ie.getMessage());
        	}        		
            log.info("Illegal argument exception caught with message: {}", ie.getMessage());
            response.setStatus(Status.CREDENTIALS_ICAR_ERROR);
            response.setDesc(ie.getMessage());
        } catch (Exception e) {
            log.error("Error general in Icar parsing service with message: {}", e.getMessage());
            response.setStatus(Status.CREDENTIALS_ICAR_ERROR);
        }
        return response;
    }

    private JSONObject fakeValidateCredentials() {
        String uuid = UUID.randomUUID().toString();
        String fake = "{\"code\":\"NOT_CORRECT\",\"documentType\":\"IDMEXE2\",\"scanId\":\""+uuid+"\",\"document\":{\"gender\":\"H\",\"documentType\":\"IDMEXE2\",\"documentNumber\":\"139767407\",\"TEST_VIZ_EXPEDITION_DATE_COHERENCE\":\"UNVALIDATED (EXPEDITION_DATE unavailable)\",\"TEST_EXPIRY_DATE\":\"OK\",\"TEST_CORRESPONDENCE_VISIBLE_MRZ_NAME\":\"FAIL\",\"TEST_CORRESPONDENCE_BARCODE_MRZ_DOC_NUMBER\":\"FAIL\",\"TEST_MRZ_FIELDS_INTEGRITY_BIRTHDATE\":\"OK\",\"TEST_MRZ_GLOBAL_INTEGRITY\":\"OK\",\"claveElector\":\""+String.valueOf(System.currentTimeMillis())+"\",\"MRZ\":\"IDMEX1397674074<<1219074163908\\n8710112H2512314MEX<02<<14480<0\\nVARELA<CORONA<<JESUS<ELIAS<<<<\",\"surname\":\"VARELA CORONA\",\"TEST_MRZ_FIELDS_INTEGRITY_DOC_NUMBER\":\"OK\",\"secondSurname\":\"CORONA\",\"TYPE\":\"IDENTITY\",\"firstSurname\":\"VARELA\",\"dateOfExpiry\":\"2025-12-31\",\"address\":\"C XOCHIATIPAN MZA 52 LT I 22\\nCCX POJO GCM,AEZ CD SAHAGL 4 439 3\\nTEPEAP11,CO. HGO\",\"TEST_CORRESPONDENCE_VISIBLE_MRZ_SURNAME\":\"OK\",\"scanId\":\"da80775b-7cec-4db7-abf2-53c45957b556\",\"issuingState\":\"MEX\",\"dateOfBirth\":\"1987-10-11\",\"TEST_MRZ_FIELDS_INTEGRITY_EXPIRY\":\"OK\",\"SIDES_NUMBER\":\"2\",\"CRC_SECTION\":\"1219074163908\",\"SIDE\":\"0\",\"nationality\":\"MEX\",\"name\":\"JESUS -ELIAS\",\"TEST_CORRESPONDENCE_BARCODE_MRZCRC_SECTION\":\"OK\",\"TEST_COLOR_IMAGE\":\"OK\"},\"description\":\"GET_DOCUMENT_ERROR\",\"status\":\"COMPLETE\"}";
        JSONObject jsonObject = new JSONObject(fake);
        return jsonObject;
    }
	
	    private JSONObject validateCredentials(List<byte[]> contents) throws IllegalArgumentException {
        try {
            JSONObject jsonResponse = icarManager.getDocumentInfo(contents, "image/jpeg", 500);
            //Alambron----------->>            
            if(jsonResponse.optString("warning").contains("DOCUMENT NOT RECOGNIZED")) {
            	throw new IllegalArgumentException("DOCUMENT NOT RECOGNIZED");
            }
            
            //JSONObject jsonResponse = fakeValidateCredentials();
            IcarDataSingleton.getInstance().getInformationMap().put(jsonResponse.getString("scanId"), jsonResponse.toString());
            IcarDataSingleton.getInstance().getImageMap().put(jsonResponse.getString("scanId") + "_front", contents.get(0));
            if (contents.size() > 1) {
                IcarDataSingleton.getInstance().getImageMap().put(jsonResponse.getString("scanId") + "_back", contents.get(1));
            }
            return jsonResponse;
        } catch (Exception e) {
            log.error("Error validating credentials with icar with message: {}", e.getMessage());
            throw new IllegalArgumentException();
        }
    }

}
