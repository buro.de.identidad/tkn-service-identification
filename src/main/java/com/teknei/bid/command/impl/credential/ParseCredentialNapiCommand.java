package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.remote.NapiCaller;
import com.teknei.bid.util.icar.IcarDataSingleton;
import com.teknei.bid.util.icar.IcarManager;
import feign.FeignException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.util.List;
import java.util.UUID;

@Component
public class ParseCredentialNapiCommand implements Command {

    @Autowired
    private NapiCaller napiCaller;

    private static final Logger log = LoggerFactory.getLogger(ParseCredentialNapiCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse response = new CommandResponse();
        try {
            JSONObject jsonBodyIcar = validateCredentials(request.getFileContent());
            if (jsonBodyIcar == null) {
                throw new IllegalArgumentException("null");
            }
            JSONObject internal = jsonBodyIcar.getJSONObject("document");
            boolean nameFound = internal.has("name");
            boolean surnameFound = internal.has("surname");
            boolean ocrFound = internal.has("MRZ") || internal.has("CRC_SECTION");
            if (nameFound == false || surnameFound == false || ocrFound == false) {
                throw new IllegalArgumentException(internal.toString());
            }
            response.setJson(jsonBodyIcar);
            response.setDesc(jsonBodyIcar.toString());
            response.setScanId(jsonBodyIcar.getString("scanId"));
            response.setStatus(Status.CREDENTIALS_ICAR_OK);

        } catch (IllegalArgumentException ie) {
            log.info("Illegal argument exception caught with message: {}", ie.getMessage());
            response.setStatus(Status.CREDENTIALS_ICAR_ERROR);
            response.setDesc(ie.getMessage());
        } catch (Exception e) {
            log.error("Error general in Icar parsing service with message: {}", e.getMessage());
            response.setStatus(Status.CREDENTIALS_ICAR_ERROR);
        }
        return response;
    }

    private JSONObject fakeValidateCredentials() {
        String uuid = UUID.randomUUID().toString();
        String fake = "{\"code\":\"NOT_CORRECT\",\"documentType\":\"IDMEXE2\",\"scanId\":\""+uuid+"\",\"document\":{\"gender\":\"H\",\"documentType\":\"IDMEXE2\",\"documentNumber\":\"139767407\",\"TEST_VIZ_EXPEDITION_DATE_COHERENCE\":\"UNVALIDATED (EXPEDITION_DATE unavailable)\",\"TEST_EXPIRY_DATE\":\"OK\",\"TEST_CORRESPONDENCE_VISIBLE_MRZ_NAME\":\"FAIL\",\"TEST_CORRESPONDENCE_BARCODE_MRZ_DOC_NUMBER\":\"FAIL\",\"TEST_MRZ_FIELDS_INTEGRITY_BIRTHDATE\":\"OK\",\"TEST_MRZ_GLOBAL_INTEGRITY\":\"OK\",\"claveElector\":\""+String.valueOf(System.currentTimeMillis())+"\",\"MRZ\":\"IDMEX1397674074<<1219074163908\\n8710112H2512314MEX<02<<14480<0\\nVARELA<CORONA<<JESUS<ELIAS<<<<\",\"surname\":\"VARELA CORONA\",\"TEST_MRZ_FIELDS_INTEGRITY_DOC_NUMBER\":\"OK\",\"secondSurname\":\"CORONA\",\"TYPE\":\"IDENTITY\",\"firstSurname\":\"VARELA\",\"dateOfExpiry\":\"2025-12-31\",\"address\":\"C XOCHIATIPAN MZA 52 LT I 22\\nCCX POJO GCM,AEZ CD SAHAGL 4 439 3\\nTEPEAP11,CO. HGO\",\"TEST_CORRESPONDENCE_VISIBLE_MRZ_SURNAME\":\"OK\",\"scanId\":\"da80775b-7cec-4db7-abf2-53c45957b556\",\"issuingState\":\"MEX\",\"dateOfBirth\":\"1987-10-11\",\"TEST_MRZ_FIELDS_INTEGRITY_EXPIRY\":\"OK\",\"SIDES_NUMBER\":\"2\",\"CRC_SECTION\":\"1219074163908\",\"SIDE\":\"0\",\"nationality\":\"MEX\",\"name\":\"JESUS -ELIAS\",\"TEST_CORRESPONDENCE_BARCODE_MRZCRC_SECTION\":\"OK\",\"TEST_COLOR_IMAGE\":\"OK\"},\"description\":\"GET_DOCUMENT_ERROR\",\"status\":\"COMPLETE\"}";
        JSONObject jsonObject = new JSONObject(fake);
        return jsonObject;
    }

    private JSONObject validateCredentials(List<byte[]> contents) throws IllegalArgumentException {
        try {
            if(contents.size() < 2){
                throw new IllegalArgumentException("Not enough parameters");
            }
            String frontB64 = Base64Utils.encodeToString(contents.get(0));
            String reverseB64 = Base64Utils.encodeToString(contents.get(1));
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", frontB64);
            jsonObject.put("idReverso", reverseB64);
            try {
                String jsonNapiResponse = napiCaller.getData(jsonObject.toString());
                JSONObject napiResponse = new JSONObject(jsonNapiResponse);
                if(napiResponse.has("estatus")){
                    return napiResponse;
                }
                String uuid = UUID.randomUUID().toString();
                JSONObject jsonResponse = new JSONObject();
                jsonResponse.put("scanId", uuid);
                jsonResponse.put("documentType", napiResponse.get("subTipo"));
                jsonResponse.put("status", "OK");
                JSONObject internalDocument = new JSONObject();
                internalDocument.put("gender", napiResponse.opt("sexo"));
                internalDocument.put("documentType", napiResponse.opt("subTipo"));
                internalDocument.put("documentNumber", napiResponse.opt("ocr"));
                internalDocument.put("claveElector", napiResponse.opt("claveElector"));
                internalDocument.put("MRZ", napiResponse.opt("ocr"));
                internalDocument.put("surname", napiResponse.opt("primerApellido") + " " + napiResponse.opt("segundoApellido"));
                internalDocument.put("firstSurname", napiResponse.opt("primerApellido"));
                internalDocument.put("secondSurname", napiResponse.opt("segundoApellido"));
                internalDocument.put("dateOfExpiry", napiResponse.opt("vigencia"));
                internalDocument.put("address", napiResponse.opt("colonia") + " " + napiResponse.opt("calle"));
                internalDocument.put("dateOfBirth", napiResponse.opt("fechaNacimiento"));
                internalDocument.put("CRC_SECTION", napiResponse.opt("ocr"));
                internalDocument.put("name", napiResponse.opt("nombres"));
                jsonResponse.put("document", internalDocument);
                return jsonResponse;
            } catch (FeignException fe) {
                log.error("Error in napi credentials with message: {}", fe.getMessage());
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            log.error("Error validating credentials with icar with message: {}", e.getMessage());
            throw new IllegalArgumentException();
        }
    }

}
