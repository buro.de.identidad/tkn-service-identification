package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieTas;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import com.teknei.bid.util.tas.TasManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Component
public class StoreTasAdditionalCredentialCommand implements Command {

	@Value("${tkn.tas.name}")
	private String tasName;
	@Value("${tkn.tas.surname}")
	private String tasSurname;
	@Value("${tkn.tas.lastname}")
	private String tasLastname;
	@Value("${tkn.tas.identification}")
	private String tasIdentification;
	@Value("${tkn.tas.date}")
	private String tasDate;
	@Value("${tkn.tas.scan}")
	private String tasScan;
	@Value("${tkn.tas.id}")
	private String tasOperationId;
	@Value("${tkn.tas.casefile}")
	private String tasCasefile;
	@Value("${tkn.tas.idType}")
	private String tasTypeId;
	@Value("${tkn.tas.anverse}")
	private String tasAnverse;
	@Value("${tkn.tas.reverse}")
	private String tasReverse;
	@Value("${tkn.tas.secondaryIdAnv}")
	private String tasSecondaryAnverse;
	@Value("${tkn.tas.secondaryIdRev}")
	private String tasSecondaryReverse;
	@Autowired
	private TasManager tasManager;
	@Autowired
	private BidTasRepository bidTasRepository;
	@Autowired
	private BidClieRepository clieRepository;
	@Autowired
	private BidScanRepository bidScanRepository;

	private static final Logger log = LoggerFactory.getLogger(StoreTasAdditionalCredentialCommand.class);

	@Override
	public CommandResponse execute(CommandRequest request) {
		String caseResult = null;
		CommandResponse response = new CommandResponse();
		response.setId(request.getId());
		try {
			byte[] firstClear = request.getFileContent().get(0);
			byte[] secondClear = null;
			if (request.getFileContent().size() > 1) {
				secondClear = request.getFileContent().get(1);
			}
			caseResult = addCaptureToCasefile(request.getScanId(), request.getId(), firstClear, secondClear);
			if (caseResult == null) {
				throw new IllegalArgumentException();
			} else {
				response.setStatus(Status.CREDENTIALS_TAS_OK);
				response.setDesc(caseResult);
				response.setDocumentId(caseResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setDesc("");
			response.setStatus(Status.CREDENTIALS_TAS_ERROR);
		}
		return response;
	}

	public String addCaptureToCasefile(String scanId, Long operationId, byte[] imageClear, byte[] secondaryImageClear)
			throws Exception {
		Map<String, String> docProperties = getMetadataMapWithScanId(scanId, operationId);
		docProperties.put(tasOperationId, String.valueOf(System.currentTimeMillis() / 1000));
		BidClieTas bidTas = bidTasRepository.findByIdClie(operationId);
		try {
			Map<String, String> docProperties2 = getMetadataMapAddress(scanId, operationId);
			tasManager.addDocument(tasSecondaryAnverse, bidTas.getIdTas(), null, docProperties2, imageClear,
					"image/jpeg", "anverse.jpeg");
			if (secondaryImageClear != null) {
				tasManager.addDocument(tasSecondaryReverse, bidTas.getIdTas(), null, docProperties2,
						secondaryImageClear, "image/jpeg", "reverse.jpeg");

			}
		} catch (Exception e) {
			log.error("Error adding capture to casefile with message:{}", e.getMessage());
		}
		return bidTas.getIdTas();
	}

	private Map<String, String> getMetadataMapAddress(String id, Long operationId) throws Exception {
		PersonData scanInfo = getPersonalDataFromScan(id, operationId);
		Map<String, String> docProperties = new HashMap<String, String>();
		docProperties.put(tasName, scanInfo.getName());
		docProperties.put(tasSurname, scanInfo.getSurename());
		docProperties.put(tasLastname, scanInfo.getSurenameLast());
		docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
		String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
		docProperties.put(tasDate, dateISO8601);
		return docProperties;
	}

	private Map<String, String> getMetadataMapWithScanId(String id, Long operationId) throws Exception {
		Map<String, String> docProperties = getMetadataMapAddress(id, operationId);
		docProperties.put(tasScan, id);
		return docProperties;
	}

	public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception {
		PersonData personalData = new PersonData();
		BidClie bidClie = clieRepository.findOne(operationId);
		personalData.setPersonalNumber(String.valueOf(operationId));
		String name = bidClie.getNomClie() == null ? "" : bidClie.getNomClie();
		String surname = bidClie.getApePate() == null ? "" : bidClie.getApePate();
		String surnamelast = bidClie.getApeMate() == null ? "" : bidClie.getApeMate();
		String surnames = surname + " " + surnamelast;
		personalData.setName(name);
		personalData.setSurename(surname);
		personalData.setSurenameLast(surnamelast);
		personalData.setLastNames(surnames);
		return personalData;
	}

	public void addIneValidationToTas(byte[] contract, Long id, int count) throws Exception {
		BidScan bidScan = bidScanRepository.findByIdRegi(id);
		BidClieTas bidTas = bidTasRepository.findByIdClie(id);
		Map<String, String> docProperties = getMetadataMapAddress(bidScan.getScanId(), id);
		String docNumber = docProperties.get(tasIdentification);
		tasManager.addDocument("bid:Captura", bidTas.getIdTas(), null, docProperties, contract, "application/pdf",
				"verificacionIne-" + docNumber + "_" + count + ".pdf");
	}

}