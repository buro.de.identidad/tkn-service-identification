package com.teknei.bid.command;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class CommandRequest implements Serializable{

    private static final Logger log = LoggerFactory.getLogger(CommandRequest.class);

    private List<MultipartFile> files;
    private List<byte[]> fileContent;
    private Long id;
    private String scanId;
    private String documentId;
    private RequestType requestType;
    private String data;
    private String curp;
    private Status requestStatus;
    private String username;

    public void setFiles(List<MultipartFile> files){
        this.files = files;
        List<byte[]> fContents = new ArrayList<>();
        files.forEach(f -> {
            try {
                fContents.add(f.getBytes());
            } catch (IOException e) {
                log.error("Error obtaining byte content from file {} with message: {}", f.getName(), e.getMessage());
            }
        });
        fileContent = fContents;
    }
}